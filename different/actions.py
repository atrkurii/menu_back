import models
from random import randint

# all of the logic for working with DB, function names are self-explainatory.

def output(dishes):
    dish_schema = models.DishSchema()
    output = []
    for item in dishes:
        output.append(dish_schema.dump(item))
    return output

def day_to_json(day_id):
    day = models.Days.query.filter_by(id=day_id).first()
    day_created_at = day.created_at
    menu_id_breakfast = day.menu_id_breakfast
    dish_id = models.menuDishes.query.filter_by(menu_id = menu_id_breakfast).first()
    dish = models.Dish.query.filter_by(id = dish_id.dish_id).all()
    output_breakfast = output(dish)
    menu_id_lunch = day.menu_id_lunch
    dish_id = models.menuDishes.query.filter_by(menu_id = menu_id_lunch).first()
    dish = models.Dish.query.filter_by(id = dish_id.dish_id).all()
    output_lunch = output(dish)
    menu_id_dinner = day.menu_id_dinner
    dish_id = models.menuDishes.query.filter_by(menu_id = menu_id_dinner).first()
    dish = models.Dish.query.filter_by(id = dish_id.dish_id).all()
    output_dinner = output(dish)
    final_output = {'day_id': day_id, 'created_at' : day_created_at, 'breakfast' : output_breakfast, 'lunch' : output_lunch, 'dinner' : output_dinner}
    return final_output
  
def return_random_menu():
    menus = models.Menu.query.all()
    dish_id = models.menuDishes.query.filter_by(menu_id = randint(1, menus[-1].id)).first()
    dish = models.Dish.query.filter_by(id = dish_id.dish_id).all()
    return(output(dish))

def return_all_menus():
    menus = models.Menu.query.all()
    menuDishes = None
    final_output = []
    for menu in menus:
        menuDishes = models.menuDishes.query.filter_by(menu_id = menu.id).all()
        dishes = []
        for dish in menuDishes:
            dishes_item = models.Dish.query.filter_by(id = dish.dish_id).first()
            dishes.append({'dish_id' : dishes_item.id, 'type':dishes_item.type, 'title_UA':dishes_item.title_UA, 'title_EN':dishes_item.title_EN, 'taste': dishes_item.taste, 'saturation':dishes_item.saturation, 'recipe_url':dishes_item.recipe_url})
        final_output.append({'title' : menu.title, 'id' : menu.id, 'created_at' : menu.created_at, 'is_featured' : menu.is_featured, 'dishes' : dishes})
    return final_output

def return_random_dish():
    dishes = models.Dish.query.all()
    random_id = randint(0, dishes[-2].id)
    dish = dishes[random_id]
    return({'id' : dish.id, 'type' : dish.type, 'title_UA' : dish.title_UA, 'title_EN' : dish.title_EN, 'taste' : dish.taste, 'saturation' : dish.saturation, 'recipe_url' : dish.recipe_url})

def return_all_dishes():
    dishes = models.Dish.query.all()
    return(output(dishes))

def create_menu(dish_ids, menu_title, is_featured = True):
    print(type(dish_ids))
    if type(dish_ids) == list:
        item_id = 0
        for item in dish_ids:
            created_menu = models.Menu(title=menu_title, is_featured=is_featured)
            dish = models.Dish.query.filter_by(id = int(item)).first()
            created_menuDishes = models.menuDishes()
            created_menuDishes.dish_backpop = dish
            created_menu.dishes.append(created_menuDishes)
            models.menuDishes.add_commit(created_menuDishes)
            models.Menu.add_commit(created_menu)
            item_id += 1
        return f'menu "{menu_title}" was created with dishes {dish_ids}'
    elif type(dish_ids) == int:
        created_menu = models.Menu(title=menu_title, is_featured=is_featured)
        dish = models.Dish.query.filter_by(id = dish_ids).first()
        created_menuDishes = models.menuDishes()
        created_menuDishes.dish_backpop = dish
        created_menu.dishes.append(created_menuDishes)
        models.menuDishes.add_commit(created_menuDishes)
        models.Menu.add_commit(created_menu)
    return f'menu "{menu_title}" was created with dish {dish_ids}'

def generate_random_day(title = 'no user title'):
    menus = models.Menu.query.all()
    created_day = models.Days(title = title, menu_id_breakfast=menus[randint(1, menus[-1].id)].id, menu_id_lunch=menus[randint(1, menus[-1].id)].id, menu_id_dinner=menus[randint(1, menus[-1].id)].id)
    models.Days.add_commit(created_day)
    
def return_last_seven_days():
    days = models.Days.query.all()
    id = days[-1].id
    final_output = []
    if id >= 7:
        while id != days[-1].id - 7:
            final_output.append(day_to_json(id))
            id = id-1
    return final_output

def add_dish(type, title_UA, title_EN, taste, saturation, recipe_url):
    created_dish = models.Dish(type=type, title_UA=title_UA, title_EN=title_EN, taste = int(taste), saturation = int(saturation), recipe_url=recipe_url)
    models.Days.add_commit(created_dish)
    return f'dish of type {type}, {title_EN}, was added'