from xml.etree.ElementInclude import default_loader
from app import db, ma
import datetime

# defining table models in our DB:

class menuDishes(db.Model):
    __tablename__ = 'menu_dish'
    menu_id = db.Column(db.Integer, db.ForeignKey('menus.id'), primary_key = True)
    dish_id = db.Column(db.Integer, db.ForeignKey('dishes.id', primary_key = True))
    menu_backpop = db.relationship('Menu', back_populates = 'dishes')
    dish_backpop = db.relationship('Dish', back_populates = 'menus')

    def add_commit (model):
        db.session.add(model)
        db.session.commit()
        db.session.refresh(model)

    def __init__ (self):
        pass

    def __repr__ (self):
        return self


class Days (db.Model):
    __tablename__ = 'days'
    id = db.Column('id', db.Integer, primary_key=True, unique=True, autoincrement=True)
    created_at = db.Column(db.DateTime(), nullable=False)
    title = db.Column(db.String(80), nullable = False)
    menu_id_breakfast = db.Column(db.Integer, db.ForeignKey('menus.id'))
    menu_id_lunch = db.Column(db.Integer, db.ForeignKey('menus.id'))
    menu_id_dinner = db.Column(db.Integer, db.ForeignKey('menus.id'))

    def add_commit (item):
        db.session.add(item)
        db.session.commit()
        db.session.refresh(item)
    
    def __init__(self, menu_id_breakfast, menu_id_lunch, menu_id_dinner, title):
        self.title = title
        self.menu_id_breakfast = menu_id_breakfast
        self.menu_id_lunch =  menu_id_lunch
        self.menu_id_dinner = menu_id_dinner
        self.created_at=datetime.datetime.now()
    
    def __repr__(self):
        return self


class Menu (db.Model):
    __tablename__ = 'menus'
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(80), nullable=False)
    created_at = db.Column(db.DateTime(), nullable=False)
    is_featured = db.Column(db.Boolean())
    dishes = db.relationship('menuDishes', back_populates = 'menu_backpop')

    def add_commit (item):
        db.session.add(item)
        db.session.commit()
        db.session.refresh(item)

    def refresh(item):
        db.session.refresh(item)

    def __init__(self, title, is_featured):
        self.title=title
        self.is_featured=is_featured
        self.created_at=datetime.datetime.now()
    
    def __repr__(self):
        return self


class Dish (db.Model):
    __tablename__ = 'dishes'
    id = db.Column('id', db.Integer, unique = True, primary_key=True, autoincrement=True)
    type = db.Column(db.String(80), nullable = False)
    title_UA = db.Column(db.String(80), nullable=False)
    title_EN = db.Column(db.String(80), nullable=False)
    taste = db.Column(db.Integer, primary_key=True)
    saturation = db.Column(db.Integer, nullable = False)
    recipe_url = db.Column(db.String(200))
    menus = db.relationship('menuDishes', back_populates = 'dish_backpop')

    def add_commit (item):
        db.session.add(item)
        db.session.commit()
        db.session.refresh(item)

    def __init__(self, type, title_UA, title_EN, taste, recipe_url, saturation):
        self.type=type
        self.title_UA=title_UA
        self.title_EN=title_EN
        self.taste=taste
        self.saturation = saturation
        self.recipe_url=recipe_url

    def __repr__(self):
        return self


class DishSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Dish
        # fields = ('id', 'type', 'title_UA', 'title_EN', 'taste', 'recipe_url')


db.create_all()