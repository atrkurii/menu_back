import config
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

import actions

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = config.DB_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
ma = Marshmallow (app)

import models


# all of the routes placed here for now, mostly for testing, whether it works or not

@app.route('/')
def hello_world():
    created_dish = models.Dish(type='dish_type', title_UA="dish title UA", title_EN="dish titke EN", taste = 3, saturation = 4, recipe_url='WWW.url.ORG')
    created_menu = models.Menu(title='menu title', is_featured=True)
    created_menuDishes = models.menuDishes()
    created_menuDishes.dish_backpop = created_dish
    created_menu.dishes.append(created_menuDishes)
    models.Menu.add_commit(created_menu)
    created_day = models.Days(title = 'day_title', menu_id_breakfast=created_menu.id, menu_id_lunch=created_menu.id, menu_id_dinner=created_menu.id)
    models.Dish.add_commit(created_dish)
    models.Days.add_commit(created_day)
    models.menuDishes.add_commit(created_menuDishes)

    return f'hello maafaka'

@app.route('/last_7_days')
def last_7_days():
    return jsonify({'days' : actions.return_last_seven_days()})

@app.route('/random_dish')
def random_dish():
    return jsonify({'dish': actions.return_random_dish()})

@app.route('/all_dishes')
def all_dishes():
    return jsonify({'dishes' : actions.return_all_dishes()})

@app.route('/all_menus')
def all_menus():
    return jsonify({'menus' : actions.return_all_menus()})

@app.route('/json')
def json():
    day_id = 3
    return jsonify(actions.day_to_json(day_id))

@app.route('/generate_day', methods=['POST'])
def generate_day():
    actions.generate_random_day()
    return 'ok', 200


@app.route('/random_menu')
def random_menu():
    return jsonify({'random_menu' : actions.return_random_menu()})


@app.route('/create_menu')
def createmenu():
    dish_ids = ["1",'4','7','3']
    menu_title = 'my menu'
    is_featured = False
    return (actions.create_menu(dish_ids, menu_title, is_featured))


@app.route('/form-example', methods=['GET', 'POST'])
def form_example():
    # handle the POST request
    if request.method == 'POST':
        language = request.form.get('language')
        framework = request.form.get('framework')
        return '''
                  <h1>The language value is: {}</h1>
                  <h1>The framework value is: {}</h1>'''.format(language, framework), 201
                

    # otherwise handle the GET request
    return '''
           <form method="POST">
               <div><label>Language: <input type="text" name="language"></label></div>
               <div><label>Framework: <input type="text" name="framework"></label></div>
               <input type="submit" value="Submit">
           </form>'''

if __name__ == '__main__':
    
    print('if__name__test')
    app.run(debug = True)